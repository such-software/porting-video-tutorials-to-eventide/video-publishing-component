module Foo
  def self.included(cls)
    cls.extend OtherThing
  end

  def call
    puts 'Foo called'
  end

  module OtherThing
    def other_thing
      puts "other thing called"
    end
  end
end

class Bar
  include Foo
end

bar = Bar.new
bar.()

Bar.other_thing()
