require_relative './interactive_init'

publish_video = Controls::Commands::PublishVideo.example
video_id = publish_video.video_id

write = Messaging::Postgres::Write.build

stream_name = "videoPublishing:command-#{video_id}"

write.(publish_video, stream_name)
