require_relative '../automated_init'

context "VideoPublishing" do
  context "published?" do
    video_publishing = Controls::VideoPublishing::New.example

    test "It thinks it is not published" do
      refute(video_publishing.published?)
    end
  end
end
