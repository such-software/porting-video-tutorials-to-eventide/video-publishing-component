require_relative '../automated_init'

context "VideoPublishing" do
  context "published?" do
    video_publishing = Controls::VideoPublishing::Published.example

    test "It thinks it's published" do
      assert(video_publishing.published?)
    end
  end
end
