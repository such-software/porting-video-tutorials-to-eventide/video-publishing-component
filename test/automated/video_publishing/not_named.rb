require_relative '../automated_init'

context "VideoPublishing" do
  context "not named?" do
    video_publishing = Controls::VideoPublishing::Named.example

    sequence = video_publishing.sequence + 1

    test "It thinks it has not the name" do
      refute(video_publishing.processed?(sequence))
    end
  end
end
