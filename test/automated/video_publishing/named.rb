require_relative '../automated_init'

context "VideoPublishing" do
  context "named?" do
    video_publishing = Controls::VideoPublishing::Named.example

    sequence = video_publishing.sequence

    test "It thinks it has applied the name" do
      assert(video_publishing.processed?(sequence))
    end
  end
end
