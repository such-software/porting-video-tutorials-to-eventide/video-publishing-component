require_relative '../automated_init'

context "Project" do
  context "VideoNamed" do
    video_publishing = Controls::VideoPublishing::Published.example
    video_id = video_publishing.id

    video_id or fail

    video_named = Controls::Events::VideoNamed.example(video_id:)

    video_named.video_id or fail
    video_named.name or fail
    sequence = video_named.sequence or fail

    Projection.(video_publishing, video_named)

    context "attributes" do
      test "id is set" do
        assert(video_publishing.id == video_named.video_id)
      end

      test "name is set" do
        assert(video_publishing.name == video_named.name)
      end

      test "sequence is set" do
        assert(video_publishing.sequence == sequence)
      end
    end
  end
end
