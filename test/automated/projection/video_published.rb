require_relative '../automated_init'

context "Project" do
  context "VideoPublished" do
    # Entity before the event is applied
    video_publishing = Controls::VideoPublishing::New.example

    # Instance of the event to apply
    video_published = Controls::Events::VideoPublished.example

    video_published.video_id or fail
    video_published.owner_id or fail
    video_published.source_uri or fail
    video_published.transcoded_uri or fail

    # Apply it - invoke the projection
    Projection.(video_publishing, video_published)

    context "attributes" do
      test "id is set" do
        assert(video_publishing.id == video_published.video_id)
      end

      test "owner_id is set" do
        assert(video_publishing.owner_id == video_published.owner_id)
      end

      test "source_uri is set" do
        assert(video_publishing.source_uri == video_published.source_uri)
      end

      test "transcoded_uri is set" do
        assert(video_publishing.transcoded_uri == video_published.transcoded_uri)
      end
    end

    test "The VideoPublishing has been published" do
      assert(video_publishing.published?)
    end
  end
end
