require_relative '../../automated_init'

context "Handle commands" do
  context "NameVideo" do
    context "VideoNamed" do
      name_video = Controls::Commands::NameVideo.example
      name_video.metadata.global_position = Controls::Sequence.example

      video_id = name_video.video_id or fail
      name = name_video.name or fail
      sequence = name_video.metadata.global_position or fail

      handler = Handlers::Commands.new

      handler.(name_video)

      writer = handler.write

      video_named = writer.one_message do |event|
        event.instance_of?(Messages::Events::VideoNamed)
      end

      test "An event of type VideoNamed was written" do
        refute(video_named.nil?)
      end

      test "It is written to a videoPublishing stream" do
        written_to_stream = writer.written?(video_named) do |stream_name|
          stream_name == "videoPublishing-#{video_id}"
        end

        assert(written_to_stream)
      end

      context "attributes" do
        test "video_id" do
          assert(video_named.video_id == video_id)
        end

        test "name" do
          assert(video_named.name == name)
        end

        test "sequence" do
          assert(video_named.sequence == sequence)
        end
      end
    end
  end
end
