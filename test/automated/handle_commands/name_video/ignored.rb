require_relative '../../automated_init'

context "Handle commands" do
  context "NameVideo" do
    context "Ignored" do
      video_publishing = Controls::VideoPublishing::Named.example
      video_id = video_publishing.id

      video_id or fail
      video_publishing.sequence or fail

      name_video = Controls::Commands::NameVideo.example(video_id:)
      name_video.metadata.global_position = video_publishing.sequence

      name_video.video_id == video_id or fail

      handler = Handlers::Commands.new

      handler.store.add(video_id, video_publishing)

      handler.(name_video)

      writer = handler.write

      video_named = writer.one_message do |event|
        event.instance_of?(Messages::Events::VideoNamed)
      end

      test "No event is written" do
        assert(video_named.nil?)
      end
    end
  end
end
