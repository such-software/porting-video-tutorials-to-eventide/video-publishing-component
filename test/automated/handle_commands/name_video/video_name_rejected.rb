require_relative '../../automated_init'

context "Handle commands" do
  context "NameVideo" do
    context "VideoNameRejected" do
      name_video = Controls::Commands::NameVideo::InvalidName.example

      name_video.name or fail
      sequence = name_video.metadata.global_position or fail

      handler = Handlers::Commands.new

      handler.(name_video)

      writer = handler.write

      video_name_rejected = writer.one_message do |event|
        event.instance_of?(Messages::Events::VideoNameRejected)
      end

      test "An event of type VideoNameRejected was written" do
        refute(video_name_rejected.nil?)
      end

      context "attributes" do
        test "reason" do
          assert(video_name_rejected.reason == ValidateName::INVALID_REASONS[:name_blank])
        end
 
        test "sequence" do
          assert(video_name_rejected.sequence == sequence)
        end
      end
    end
  end
end
