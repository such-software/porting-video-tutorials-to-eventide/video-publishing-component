require_relative '../../automated_init'

context "Handle Commands" do
  context "NameVideo" do
    context "Expected version" do
      handler = Handlers::Commands.new

      video_publishing = Controls::VideoPublishing::Published.example

      name_video = Controls::Commands::NameVideo.example
      name_video.metadata.global_position = Controls::Sequence::NotHandled.example

      version = Controls::Version.example

      handler.store.add(name_video.video_id, video_publishing, version)

      handler.(name_video)

      writer = handler.write

      video_published = writer.one_message do |event|
        event.instance_of?(Messages::Events::VideoNamed)
      end

      test "Written with the entity's expected version" do
        written_to_stream = writer.written?(video_published) do |_, expected_version|
          expected_version == version
        end

        assert(written_to_stream)
      end
    end
  end
end
