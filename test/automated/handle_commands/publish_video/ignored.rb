require_relative '../../automated_init'

context "Handle Commands" do
  context "PublishVideo" do
    context "Ignored" do
      handler = Handlers::Commands.new

      video_publishing = Controls::VideoPublishing::Published.example

      video_id = video_publishing.id or fail

      publish_video = Controls::Commands::PublishVideo.example(video_id:)

      publish_video.video_id == video_id or fail

      handler.store.add(video_id, video_publishing)

      handler.(publish_video)

      writer = handler.write

      video_published = writer.one_message do |event|
        event.instance_of?(Messages::Events::VideoPublished)
      end

      test "VideoPublished Event is not Written" do
        assert(video_published.nil?)
      end
    end
  end
end
