require_relative '../../automated_init'

context "Handle Commands" do
  context "PublishVideo" do
    context "Expected version" do
      handler = Handlers::Commands.new

      video_publishing = Controls::VideoPublishing::New.example

      publish_video = Controls::Commands::PublishVideo.example

      version = Controls::Version.example

      handler.store.add(publish_video.video_id, video_publishing, version)

      handler.(publish_video)

      writer = handler.write

      video_published = writer.one_message do |event|
        event.instance_of?(Messages::Events::VideoPublished)
      end

      test "Written with the entity's expected version" do
        written_to_stream = writer.written?(video_published) do |_, expected_version|
          expected_version == version
        end

        assert(written_to_stream)
      end
    end
  end
end
