require_relative '../../automated_init'

context "Handle Commands" do
  context "PublishVideo" do
    context "VideoPublished" do
      handler = Handlers::Commands.new
   
      transcoded_uri = Controls::Uri::Transcoded.example

      handler.transcode.uri = transcoded_uri

      publish_video = Controls::Commands::PublishVideo.example

      video_id = publish_video.video_id or fail
      owner_id = publish_video.owner_id or fail
      source_uri = publish_video.source_uri or fail

      handler.(publish_video)

      writer = handler.write

      video_published = writer.one_message do |event|
        event.instance_of?(Messages::Events::VideoPublished)
      end

      test "VideoPublished Event is Written" do
        refute(video_published.nil?)
      end

      test "Written to the videoPublishing stream" do
        written_to_stream = writer.written?(video_published) do |stream_name|
          stream_name == "videoPublishing-#{video_id}"
        end

        assert(written_to_stream)
      end

      context "Attributes" do
        test "video_id" do
          assert(video_published.video_id == video_id)
        end

        test "owner_id" do
          assert(video_published.owner_id == owner_id)
        end

        test "source_uri" do
          assert(video_published.source_uri == source_uri)
        end

        test "transcoded_uri" do
          assert(video_published.transcoded_uri == transcoded_uri)
        end
      end
    end
  end
end
