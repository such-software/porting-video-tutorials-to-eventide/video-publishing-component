require_relative '../automated_init'

context "ValidateName" do
  context "valid" do
    name = Controls::VideoName.example

    valid = ValidateName.(name)

    test "The name was valid" do
      assert(valid)
    end
  end
end
