require_relative '../../automated_init'

context "ValidateName" do
  context "Invalid" do
    context "Whitespace string" do
      name = Controls::VideoName::Invalid::WhitespaceString.example

      valid, reason = ValidateName.(name)

      test "The name was invalid" do
        refute(valid)
      end

      test "The reason is because the name was blank" do
        assert(reason == 'NAME_BLANK')
      end
    end
  end
end
