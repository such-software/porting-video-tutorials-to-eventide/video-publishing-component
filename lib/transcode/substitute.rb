module Transcode
  class Substitute
    attr_writer :uri

    def call(source_uri)
      @uri
    end
  end
end
