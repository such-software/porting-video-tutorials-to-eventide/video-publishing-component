module Transcode
  class Ffmpeg
    FAKE_TRANSCODING_DESTINATION = 'https://www.youtube.com/watch?v=GI_P3UtZXAA'

    def self.build
      new
    end

    def self.configure(receiver)
      instance = build

      receiver.transcode = instance
    end

    def call(source_uri)
      puts "We totally have a video transcoder installed that we are"
      puts "totally calling in this function. If we did not have such"
      puts "an awesome one installed locally, we could call into a"
      puts "3rd-party API here instead."

      FAKE_TRANSCODING_DESTINATION
    end

    module Substitute
      def self.build
        Transcode::Substitute.new
      end
    end
  end
end
