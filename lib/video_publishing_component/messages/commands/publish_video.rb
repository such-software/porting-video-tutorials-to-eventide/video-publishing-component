module VideoPublishingComponent
  module Messages
    module Commands
      class PublishVideo
        include Messaging::Message

        attribute :video_id, String
        attribute :owner_id, String
        attribute :source_uri, String
      end
    end
  end
end
