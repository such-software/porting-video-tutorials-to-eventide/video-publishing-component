module VideoPublishingComponent
  module Messages
    module Commands
      class NameVideo
        include Messaging::Message

        attribute :video_id, String
        attribute :name, String
      end
    end
  end
end
