module VideoPublishingComponent
  module Messages
    module Events
      class VideoNamed
        include Messaging::Message

        attribute :video_id, String
        attribute :name, String
        attribute :sequence, Integer
      end
    end
  end
end
