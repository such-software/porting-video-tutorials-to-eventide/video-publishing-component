module VideoPublishingComponent
  module Messages
    module Events
      class VideoPublished
        include Messaging::Message

        attribute :video_id, String
        attribute :owner_id, String
        attribute :source_uri, String
        attribute :transcoded_uri, String
      end
    end
  end
end
