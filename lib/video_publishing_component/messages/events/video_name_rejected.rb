module VideoPublishingComponent
  module Messages
    module Events
      class VideoNameRejected
        include Messaging::Message

        attribute :video_id, String
        attribute :name, String
        attribute :reason, String
        attribute :sequence, Integer
      end
    end
  end
end
