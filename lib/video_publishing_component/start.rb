# Component initiator user guide: http://docs.eventide-project.org/user-guide/component-host.html#component-initiator

module VideoPublishingComponent
  module Start
    def self.call
      Consumers::Commands.start("videoPublishing:command")
    end
  end
end
