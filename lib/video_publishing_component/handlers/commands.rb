module VideoPublishingComponent
  module Handlers
    class Commands
      include Messaging::Handle
      include Messaging::StreamName
      include Log::Dependency
      include Messages::Commands
      include Messages::Events

      dependency :write, Messaging::Postgres::Write
      dependency :store, Store
      dependency :transcode, Transcode::Ffmpeg

      def configure(session:)
        Messaging::Postgres::Write.configure(self, session:)
        Store.configure(self)
        Transcode::Ffmpeg.configure(self)
      end

      category :video_publishing

      handle PublishVideo do |publish_video|
        video_id = publish_video.video_id
        source_uri = publish_video.source_uri

        video_publishing, version = store.fetch(video_id, include: :version)

        if video_publishing.published?
          logger.info(tag: :ignored) { "Command ignored (Command: #{publish_video.message_type}, VideoPublishing ID: #{video_id})" }
          return
        end

        transcoded_uri = transcode.(source_uri)

        video_published = VideoPublished.follow(publish_video)

        video_published.transcoded_uri = transcoded_uri

        stream_name = stream_name(video_id)

        write.(video_published, stream_name, expected_version: version)
      end

      handle NameVideo do |name_video|
        video_id = name_video.video_id

        video_publishing, version = store.fetch(video_id, include: :version)

        if (video_publishing.processed?(name_video.metadata.global_position))
          logger.info(tag: :ignored) { "Command ignored (Command: #{publish_video.message_type}, VideoPublishing ID: #{video_id})" }
          return
        end

        valid, reason = ValidateName.(name_video.name)

        if valid
          event = VideoNamed.follow(name_video)
        else
          event = VideoNameRejected.follow(name_video)
          event.reason = reason
        end

        stream_name = stream_name(video_id)

        event.sequence = name_video.metadata.global_position

        write.(event, stream_name, expected_version: version)
      end
    end
  end
end
