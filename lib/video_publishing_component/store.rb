module VideoPublishingComponent
  class Store
    include EntityStore

    category :video_publishing
    entity VideoPublishing
    projection Projection
    reader MessageStore::Postgres::Read, batch_size: 1000

    # Optional snapshotting
    snapshot EntitySnapshot::Postgres, interval: 1000
  end
end
