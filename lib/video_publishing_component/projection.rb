module VideoPublishingComponent
  class Projection
    include EntityProjection
    include Messages::Events

    entity_name :video_publishing

    apply VideoPublished do |video_published|
      SetAttributes.(video_publishing, video_published, copy: [
        { :video_id => :id },
        :owner_id,
        :source_uri,
        :transcoded_uri
      ])

      video_publishing.publishing_attempted = true
    end

    apply VideoNamed do |video_named|
      SetAttributes.(video_publishing, video_named, copy: [
        { :video_id => :id },
        :name,
        :sequence
      ])
    end
  end
end
