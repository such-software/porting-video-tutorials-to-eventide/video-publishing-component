module VideoPublishingComponent
  module Controls
    module VideoName
      def self.example
        'Grocery stores hate this guy!'
      end

      module Invalid
        module EmptyString
          def self.example
            ''
          end
        end

        module WhitespaceString
          def self.example
            "   \t \t   \n"
          end
        end

        module Nil
          def self.example
            nil
          end
        end
      end
    end
  end
end
