module VideoPublishingComponent
  module Controls
    module VideoPublishing
      def self.example
        video_publishing = VideoPublishingComponent::VideoPublishing.build

        video_publishing
      end

      def self.id
        ID.example(increment: id_increment)
      end

      def self.id_increment
        1111
      end

      module Named
        def self.example
          video_publishing = Published.example

          video_publishing.id = id
          video_publishing.sequence = Sequence.example

          video_publishing
        end

        def self.id
          ID.example(id_increment)
        end

        def self.id_increment
          111111
        end
      end

      module New
        def self.example
          VideoPublishingComponent::VideoPublishing.build
        end
      end

      module Published
        def self.example
          video_publishing = VideoPublishingComponent::VideoPublishing.build

          video_publishing.id = self.id
          video_publishing.publishing_attempted = true

          video_publishing
        end

        def self.id
          ID.example(increment: id_increment)
        end

        def self.id_increment
          11111
        end
      end
    end
  end
end
