module VideoPublishingComponent
  module Controls
    module Events
      module VideoNamed
        def self.example(video_id: nil)
          video_named = Messages::Events::VideoNamed.new

          video_named.video_id = video_id || ID.example(1)
          video_named.name = VideoName.example
          video_named.sequence = Sequence.example

          video_named
        end
      end
    end
  end
end
