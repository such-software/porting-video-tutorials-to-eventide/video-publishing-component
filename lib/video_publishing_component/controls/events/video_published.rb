module VideoPublishingComponent
  module Controls
    module Events
      module VideoPublished
        def self.example(video_id: nil)
          video_published = Messages::Events::VideoPublished.new

          video_published.video_id = video_id || ID.example(1)
          video_published.owner_id = ID.example(11)
          video_published.source_uri = Uri.example
          video_published.transcoded_uri = Uri::Transcoded.example

          video_published
        end
      end
    end
  end
end
