module VideoPublishingComponent
  module Controls
    module Sequence
      def self.example
        4
      end

      module NotHandled
        def self.example
          5
        end
      end
    end
  end
end
