module VideoPublishingComponent
  module Controls
    module Uri
      def self.example
        "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
      end

      module Transcoded
        def self.example
          "https://www.youtube.com/watch?v=NQzkTZtL1EM"
        end
      end
    end
  end
end
