module VideoPublishingComponent
  module Controls
    module Commands
      module PublishVideo
        def self.example(video_id: nil)
          publish_video = Messages::Commands::PublishVideo.new

          publish_video.video_id = video_id || ID.example(1)
          publish_video.owner_id = ID.example(11)
          publish_video.source_uri = Uri.example

          publish_video
        end
      end
    end
  end
end
