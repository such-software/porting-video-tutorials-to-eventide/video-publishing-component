module VideoPublishingComponent
  module Controls
    module Commands
      module NameVideo
        def self.example(video_id: nil)
          name_video = Messages::Commands::NameVideo.new

          name_video.video_id = video_id || ID.example
          name_video.name = VideoName.example

          name_video
        end

        module InvalidName
          def self.example
            name_video = Messages::Commands::NameVideo.new

            name_video.video_id = ID.example
            name_video.name = VideoName::Invalid::EmptyString.example

            name_video.metadata.global_position = Sequence.example

            name_video
          end
        end
      end
    end
  end
end
