require "clock/controls"
require "identifier/uuid/controls"

require "video_publishing_component/controls/id"
require "video_publishing_component/controls/time"
require "video_publishing_component/controls/version"
require "video_publishing_component/controls/uri"
require "video_publishing_component/controls/sequence"
require "video_publishing_component/controls/video_name"

require "video_publishing_component/controls/video_publishing"

require "video_publishing_component/controls/commands/publish_video"
require "video_publishing_component/controls/commands/name_video"
require "video_publishing_component/controls/events/video_published"
require "video_publishing_component/controls/events/video_named"
