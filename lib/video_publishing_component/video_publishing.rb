module VideoPublishingComponent
  class VideoPublishing
    include Schema::DataStructure

    attribute :id, String
    attribute :publishing_attempted, TrueClass
    attribute :owner_id, String
    attribute :source_uri, String
    attribute :transcoded_uri, String
    attribute :name, String
    attribute :sequence, Integer, default: 0

    def published?
      !!publishing_attempted
    end

    def processed?(sequence)
      sequence <= self.sequence
    end
  end
end
