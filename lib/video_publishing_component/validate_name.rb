module VideoPublishingComponent
  module ValidateName
    INVALID_REASONS = {
      :name_blank => 'NAME_BLANK'
    }

    def self.call(name)
      normalized_name = name.nil? ? '' : name.strip

      '' == normalized_name ? [false, INVALID_REASONS[:name_blank]] : true
    end
  end
end
