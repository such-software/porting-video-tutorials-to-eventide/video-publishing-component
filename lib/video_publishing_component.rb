require "eventide/postgres"
require "transcode"

require "video_publishing_component/messages/commands/publish_video"
require "video_publishing_component/messages/commands/name_video"
require "video_publishing_component/messages/events/video_published"
require "video_publishing_component/messages/events/video_named"
require "video_publishing_component/messages/events/video_name_rejected"

require "video_publishing_component/video_publishing"
require "video_publishing_component/projection"
require "video_publishing_component/store"
require "video_publishing_component/validate_name"

require "video_publishing_component/handlers/commands"

require "video_publishing_component/consumers/commands"

require "video_publishing_component/start"
